#include <Arduino.h>
#include <TFMPlus.h>

TFMPlus lidar;

U16 dist;
U16 _;

void setup()
{
    /** Serial to computer **/
    Serial.begin(115200);

    /** TFMiniPlus **/
    Serial1.begin(115200);
    if (!lidar.begin(&Serial1)) {
        Serial.println("Error de conexion con lidar");
        Serial.println("Conecta el sensor y presiona reset");
        while(true) ;
    }
    lidar.sendCommand(SET_FRAME_RATE, FRAME_0);
    lidar.sendCommand(STANDARD_FORMAT_CM, 0);
}

void loop()
{
    lidar.sendCommand(TRIGGER_DETECTION, 0);
    if (lidar.getData(dist, _, _)) {
        Serial.print("Distancia: ");
        Serial.print(dist);
        Serial.println(" cm");
    } else {
        Serial.println("Error en la lectura");
    }
    delay(1000);
}
